package tinshoes.util;
import java.util.Scanner;
import java.util.ArrayList;
import tinshoes.input.SafeScanner;
import java.io.File;
import java.io.PrintWriter;
public class GenClass {
	static public void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		SafeScanner ssc = new SafeScanner(sc);
		System.out.println("What package should the class be in?");
		String pkg = sc.nextLine();
		String pkgPath = pkg.replace('.', File.separatorChar);
		boolean dfltPkg = pkg.equals("");
		if (pkg.equals(""))
			System.err.println("Using default package.\nCAUTION: Default packages are discouraged for all classes, and impossible for non-main classes.");
		else {
			if (!(new File(pkgPath).exists())) {
				System.err.println(pkg + " does not yet exist. Creating folder " + pkgPath + ".");
				new File(pkgPath).mkdirs();
			}
			System.out.println("Package name: " + pkg);
			System.out.println("Package path: " + pkgPath);
		}
		String classn = "";
		System.out.println("What is the name of the class?");
		do {
			classn = sc.nextLine();
		} while (classn.equals(""));
		File classf = new File((dfltPkg ? "" : pkgPath + File.separator) + classn + ".java");
		try {
			if (!classf.createNewFile())
				if (!ssc.yn(classn + ".java already exists. Would you like to overwrite it?", "That is not a valid answer."))
					return;
		} catch (Exception e) {
			System.out.println("An exception occurred: " + e.getMessage());
			System.err.println("Exception stack trace: " + e.getStackTrace());
		}
		System.out.println("Empty " + classn + ".java was created.");
		ArrayList<String> imports = new ArrayList<String>();
		System.out.println("List all imports for use in the class.");
		String curr = "";
		do {
			curr = sc.nextLine();
			if (curr.equals("")) break;
			imports.add(curr);
		} while (true);
		ArrayList<String[]> vars = new ArrayList<String[]>();
		System.out.println("List all non-static variables for use in the class.");
		do {
			curr = sc.nextLine();
			if (curr.equals("")) break;
			vars.add(splitWords(curr));
		} while (true);
		//System.out.println(vars);
		ArrayList<String[]> staticVars = new ArrayList<String[]>();
		System.out.println("List all static variables for use in the class.");
		do {
			curr = sc.nextLine();
			if (curr.equals("")) break;
			staticVars.add(splitWords(curr));
		} while (true);
		System.out.println("Generating class...");
		try {
			PrintWriter writer =  new PrintWriter(classf);
			if (!dfltPkg)
				writer.println("package " + pkg + ";");
			for (String importable : imports)
				writer.println("import " + importable + ";");
			writer.println("public class " + classn + " {");
			System.out.println("Generating variables...");
			for (String[] var : staticVars)
				writer.println("\tstatic " + var[0] + " " + var[1] + ";");
			for (String[] var : vars)
				writer.println("\t" + var[0] + " " + var[1] + ";");
			if (ssc.yn("Generate constructor?", "That is not valid input.")) {
				writer.print("\tpublic " + classn + "(");
				for (int i = 0; i < vars.size(); i++)
					writer.print(vars.get(i)[0] + " " + vars.get(i)[1] + (i == vars.size()-1 ? "" : ", "));
				writer.println(") {");
				for (String[] var : vars) {
					writer.println("\t\tthis." + var[1] + " = " + var[1] + ";");
				}
				writer.println("\t}");
			}
			if (ssc.yn("Generate main()?", "That is not valid input."))
				writer.println("\tpublic static void main(String[] args) {\n\t}");
			System.out.println("Generating get/set methods...");
			for (String[] var : vars) {
				writer.println("\tpublic void set" + capitalize(var[1]) + "(" + var[0] + " " + var[1] + ") {");
				writer.println("\t\tthis." + var[1] + " = " + var[1] + ";\n\t}");
				writer.println("\tpublic " + var[0] + " get" + capitalize(var[1]) + "() {");
				writer.println("\t\treturn this." + var[1] + ";\n\t}");
			}
			writer.println("}");
			writer.flush();
			writer.close();
		} catch (Exception e) {
			System.out.println("An exception occurred: " + e.getMessage());
			System.err.println("Exception stack trace: " + e.getStackTrace());
			return;
		}

	}
	private static String capitalize(String s) {
		return Character.toString(Character.toUpperCase(s.charAt(0))) + s.substring(1, s.length());
	}
	private static String[] splitWords(String s) {
		int space = s.indexOf(' ');
		return space == -1 ? new String[] {"", ""} : new String[] {s.substring(0, space), s.substring(space + 1, s.length())};
	}
}
