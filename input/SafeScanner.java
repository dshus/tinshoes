package tinshoes.input;
import java.util.Scanner;
//Yes, I know I could extend Scanner. I just don't have to deal with all those overloaded constructors. Please don't judge me, I'm just lazy.
public class SafeScanner{
	private Scanner sc;
	public int scans = 0;
	public SafeScanner(Scanner sc) {
		this.sc = sc;
	}
	//public void test() {
	//	System.out.println(this.sc.next());
	//}
	public boolean yn(String question, String error) {
		System.out.println(question);
		do {
			switch (this.sc.next().toLowerCase()) {
				case "y":
				case "yes":
				case "yeah":
				case "yep":
					scans++;
					return true;
				case "n":
				case "no":
				case "nah":
				case "nope":
					scans++;
					return false;
				default:
					System.err.println(error);
					break;
			}
		} while (true);
	}
	public int nextInt(String question, String error, boolean prepend) {
		System.out.println(question);
		while (true) {
			if (sc.hasNextInt()) {
				scans++;
				return sc.nextInt();
			}
			else {
				String next = sc.next();
				System.err.println(prepend ? next + error : error);
			}
		}
	}
	public float nextFloat(String question, String error, boolean prepend) {
                System.out.println(question);
                while (true) {
                        if (sc.hasNextFloat()) {
				scans++;
                                return sc.nextFloat();
			}
                        else {
				String next = sc.next();
                                System.err.println(prepend ? next + error : error);
			}
                }
        }
	public double nextDouble(String question, String error, boolean prepend) {
                System.out.println(question);
                while (true) {
                        if (sc.hasNextDouble()) {
				scans++;
                                return sc.nextDouble();
			}
                        else {
				String next = sc.next();
                                System.err.println(prepend ? next + error : error);
			}
				
                }
        }
	
}
