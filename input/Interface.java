package tinshoes.input;
public class Interface {
	public static String th(int x) {
		String str = Integer.toString(x);
		char f = str.charAt(str.length() - 1);
		//REMOVED FOR READABILITY: return f == '1' ? "st" : (f == '2' ? "nd" : (f == '3' ? "rd" : "th"));
		switch (f) {
			case '1': return "st";
			case '2': return "nd";
			case '3': return "rd";
			default: return "th";
		}
	}
}