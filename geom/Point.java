package tinshoes.geom;
public class Point {
	public double x, y;
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
	public double slope(Point p) {
		return this.x - p.x == 0 ? (double)-1/(double)0 : (this.y - p.y)/(this.x - p.x);
	}
	public static double slope(Point p, Point q) {
		return q.x - p.x == 0 ? (double)-1/(double)0 : (q.y - p.y)/(q.x - p.x);
	}
	public Point midpoint(Point p) {
		return new Point((this.x + p.x) / 2, (this.y + p.y) / 2);
	}
	public static Point midpoint(Point p, Point q) {
		return new Point((q.x + p.x) / 2, (q.y + p.y) / 2);
	}
	public double length(Point p) {
		return Math.sqrt(Math.pow(this.x - p.x, 2) + Math.pow(this.y - p.x, 2));
	}
	public static double length(Point p, Point q) {
		return Math.sqrt(Math.pow(q.x - p.x, 2) + Math.pow(q.y - p.x, 2));
	}
}
