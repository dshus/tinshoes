package tinshoes.geom;
import tinshoes.geom.Point;
public class Circle {
	private double radius, diameter, circumfrence, area;
	private Point center;
	private String name;
	public enum valType {
		RADIUS, DIAMETER, CIRCUMFRENCE, AREA
	}
	public Circle(Point center, Point border, String name) {
		this(Point.length(center, border), Circle.valType.RADIUS, center, name);
	}
	public Circle(double val, Circle.valType valT, Point center, String name) {
		setAndCalc(val, valT);
		this.name = name;
		this.center = center;
	}
	public void setAndCalc(double val, Circle.valType t) {
		switch (t) {
			case RADIUS:
				radius = val;
				diameter = 2 * radius;
				circumfrence = diameter * Math.PI;
				area = Math.PI * Math.pow(radius, 2);
				break;
			case DIAMETER:
				diameter = val;
				radius = diameter / 2;
				circumfrence = diameter * Math.PI;
				area = Math.PI * Math.pow(radius, 2);
				break;
			case CIRCUMFRENCE:
				circumfrence = val;
				diameter = circumfrence / Math.PI;
				radius = diameter / 2;
				area = Math.PI * Math.pow(radius, 2);
				break;
			case AREA:
				area = val;
				radius = Math.sqrt(area / Math.PI);
				diameter = 2 * radius;
				circumfrence = diameter * Math.PI;
				break;
		}
	}
	public void setCenter(Point center) {
		this.center = center;
	}
	public Point getCenter() {
		return this.center;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
	public double getDiameter() {
		return this.diameter;
	}
	public double getRadius() {
		return this.radius;
	}
	public double getCircumfrence() {
		return this.circumfrence;
	}
	public double getArea() {
		return this.area;
	}
	public String toString() {
		return "Name: " + name + "\nRadius: " + radius + "\nDiameter: " + diameter + "\nCircumfrence: " + circumfrence + "\nArea: " + area;
	}
}
