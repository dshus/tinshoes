package tinshoes.geom;
import tinshoes.geom.Point;
import tinshoes.geom.Line;
public class Rectangle {
	public Point[] points = new Point[4];
	private Line[] possibleLines = new Line[6];
	private Line[] useLines = new Line[4];
	private int[][] linePoints = {{0, 1}, {2, 3}, {1, 2}, {0, 3}, {0, 2}, {1, 3}};
	private int[][] pairLines = new int[2][2];
	public Rectangle (Point p, Point s) {
		this(p, new Point(p.x, s.y), s, new Point(s.x, p.y));
	}
	public Rectangle (Point p, Point q, Point r, Point s) {
		this.points[0] = p;
		this.points[1] = q;
		this.points[2] = r;
		this.points[3] = s;
		Line temp = new Line(points[0], points[2]);
		Line temp2 = new Line(points[1], points[3]);
		if (temp.evalPoint(points[1]) == 0 || temp.evalPoint(points[3]) == 0 || temp2.evalPoint(points[0]) == 0 || temp2.evalPoint(points[2]) == 0) {
			throw new IllegalArgumentException("Two or more points are collinear.");
		}
		//Hardcoded-ish because it'll honestly take more time to figure out how to do it with a for loop than to hardcode the assignment.
		possibleLines[0] = new Line(points[0], points[1]);
		possibleLines[1] = new Line(points[2], points[3]);
		possibleLines[2] = new Line(points[1], points[2]);
		possibleLines[3] = new Line(points[0], points[3]);
		possibleLines[4] = new Line(points[0], points[2]);
		possibleLines[5] = new Line(points[1], points[3]);

		for (int j = 0; j < 6; j++) {
			int[] ptsNotInc = notIncludes(linePoints[j][0], linePoints[j][1], 4);
			if (possibleLines[j].eitherSide(points[ptsNotInc[0]], points[ptsNotInc[1]])) {
				if (j == 0 || j == 1) {
					for (int k = 0; k < 4; k++) {
						useLines[k] = possibleLines[k+2];
					}
				}
				if (j == 2 || j == 3) {
					useLines[0] = possibleLines[0];
					useLines[1] = possibleLines[1];
					useLines[2] = possibleLines[4];
					useLines[3] = possibleLines[5];
				}
				if (j == 4 || j == 5) {
					for (int k = 0; k < 4; k++) {
						useLines[k] = possibleLines[k];
					}
				}
				break;
			}
		}
		boolean isValid = false;
		for (int l = 1; l < 4; l++) {
			if (useLines[0].isPerpendicularTo(useLines[l])) {
				int[] otherLns = notIncludes(0, l, 4);
				if (!useLines[0].isPerpendicularTo(useLines[otherLns[0]]) && !useLines[0].isPerpendicularTo(useLines[otherLns[1]]))
					continue;
				pairLines[0][0] = 0;
				pairLines[0][1] = otherLns[useLines[0].isParallelTo(useLines[otherLns[0]]) ? 0 : 1];
				pairLines[1][0] = l;
				pairLines[1][1] = otherLns[useLines[l].isParallelTo(useLines[otherLns[0]]) ? 0 : 1];
				isValid = true;
			}
		}
		if (!isValid)
			throw new IllegalArgumentException("The rectangle is not valid.");
		/*System.err.println(pairLines[0][0]);
		System.err.println(pairLines[0][1]);
		System.err.println(pairLines[1][0]);
		System.err.println(pairLines[1][1]);*/
	}
	public boolean contains(Point x) {
		return useLines[pairLines[0][0]].pointIsBetween(useLines[pairLines[0][1]], x) && useLines[pairLines[1][0]].pointIsBetween(useLines[pairLines[1][1]], x);
	}
	public boolean isParallelToAxes() {
		for (Line l : useLines) {
			if (l.slope() == 0)
				return true;
		}
		return false;
	}
	public String toString() {
		/*String ans = "";
		for (int z = 0; z < 4; z++) {
			ans += this.points[z].toString();
			if (z != 3) {
				ans += ", ";
			}
		}
		return ans;*/
		return "Line Pair 1: [" + useLines[pairLines[0][0]].toString() + "], [" + useLines[pairLines[0][1]].toString() + "] (m = " + (useLines[pairLines[0][1]].slope() == (double)-1/(double)0 ? "undefined" : useLines[pairLines[0][1]].slope()) + ")\nLine Pair 2: [" + useLines[pairLines[1][0]].toString() + "], [" + useLines[pairLines[1][1]].toString() + "] (m = " + (useLines[pairLines[1][1]].slope() == (double)-1/(double)0 ? "undefined" : useLines[pairLines[1][1]].slope()) + ")";
	}
	private static int[] notIncludes(int a, int b, int limit) {
		int[] ans = new int[limit - 2];
		int count = 0;
		for (int i = 0; i < limit; i++) {
			if (i != a && i != b) {
				ans[count] = i;
				count++;
			}
		}
		return ans;
	}
	
}
