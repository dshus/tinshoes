package tinshoes.geom;
import tinshoes.geom.Point;
public class Line {
	private Point p, q;
	//private double slope;
	public Line(Point p, Point q) {
		this.p = p;
		this.q = q;
	}
	public double slope() {
		return p.slope(q);
	}
	public Point yInt() {
		if (this.slope() == (double)-1/(double)0) {
			throw new IllegalArgumentException("The line is vertical and does not have exactly one y-intercept.");
		}
		return new Point(0, (-this.slope() * p.x) + p.y);
	}
	public boolean isPerpendicularTo(Line l) {
		return -1/this.slope() == l.slope();
	}
	public boolean isParallelTo(Line l) {
		return this.slope() == l.slope();
	}

	//Usage: Use the inequality/equality sign with 0.
	//Ex: x.evalPoint(p) >= 0 will return true if point is greater than or equal the line
	public int evalPoint(Point t) {
		if (this.slope() == (double)-1/(double)0) {
			return t.x == p.x ? 0 : (t.x > p.x ? 1 : -1);
		}
		return t.y == (this.slope() * t.x) + this.yInt().y ? 0 : (t.y > (this.slope() * t.x) + this.yInt().y ? 1 : -1);
	}
	public boolean pointIsBetween(Line l, Point t) {
		if (!this.isParallelTo(l)) {
			throw new IllegalArgumentException("The lines are not parallel.");
		}
		if (this.slope() == (double)-1/(double)0)
			return this.p.x > l.getPoint1().x ? (t.x <= this.p.x && t.x >= l.getPoint1().x) : (t.x <= l.getPoint1().x && t.x >= this.p.x); 
		if (this.yInt().y > l.yInt().y)
			return this.evalPoint(t) <= 0 && l.evalPoint(t) >= 0;
		else
			return l.evalPoint(t) <= 0 && this.evalPoint(t) >= 0;
	}
	public Point getPoint1() {
		return p;
	}
	public Point getPoint2() {
		return q;
	}
	public boolean eitherSide(Point x, Point y) {
		return (this.evalPoint(x) > 0 && this.evalPoint(y) < 0) || (this.evalPoint(x) < 0 && this.evalPoint(y) > 0);
	}
	public String toString() { 
		return p.toString() + ", " + q.toString();
	}
}
