#Tinshoes -- It's the little things.
Tinshoes stands for **Tin**y **Sho**rtcut **Es**sentials.

It's a small, personal Java library that saves time in the long-run.

Hopefully.

#Usage
Every package is a subpackage to tinshoes. These subpackages contain actual classes.

Simply include the library in your project or classpath and import the classes necessary.

Example:
    import tinshoes.input.SafeScanner;
