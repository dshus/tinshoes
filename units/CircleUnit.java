package tinshoes.units;
import tinshoes.geom.Circle;
import tinshoes.geom.Point;
public class CircleUnit {
	public static void main(String[] args) {
		Circle c = new Circle(1, Circle.valType.RADIUS, new Point(0, 0), "Unit");
		System.out.println(c.getName());
		System.out.println(c.getDiameter());
		System.out.println(c.getCircumfrence());
		System.out.println(c.getArea());
		c.setName("c");
		System.out.println(c.getName());
		c.setAndCalc(40, Circle.valType.AREA);
		System.out.println(c.getDiameter());
		System.out.println(c.getCircumfrence());
		System.out.println("=========================================================");
		c = new Circle(new Point(0, 0), new Point(0, 1), "Unit");
		System.out.println(c.getName());
		System.out.println(c.getDiameter());
		System.out.println(c.getCircumfrence());
		System.out.println(c.getArea());
		c.setName("c");
		System.out.println(c.getName());
		c.setAndCalc(40, Circle.valType.AREA);
		System.out.println(c.getDiameter());
		System.out.println(c.getCircumfrence());
	}
}