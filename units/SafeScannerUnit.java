package tinshoes.units;
import java.util.Scanner;
import tinshoes.input.SafeScanner;

public class SafeScannerUnit {
	public static void main(String[] args) {
		Scanner usc = new Scanner(System.in);
		SafeScanner sc = new SafeScanner(usc);
		System.out.println(sc.yn("Yes or no?", "That is not a valid answer."));
		System.out.println(sc.nextDouble("Enter a double.", " is not a double.", true));
		System.out.println(sc.nextInt("Enter an integer.", "That is not an integer.", false));
		System.out.println(sc.scans);
	}
}
